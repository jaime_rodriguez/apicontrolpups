<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/', 'UserController@index');
Route::get('/user', 'UserController@user')->middleware('auth:api');

/** CRIAS */
Route::post('pups/save', 'Pups\PupsController@save');
Route::get('pups/getAll','Pups\PupsController@getAll');
Route::get('pups/getAllNoClass','Pups\PupsController@getAllNoClass');
Route::get('pups/getAllSick','Pups\PupsController@getAllSick');
Route::get('pups/getById/{id}','Pups\PupsController@getById');
Route::put('pups/{idPup}','Pups\PupsController@update');
Route::delete('pups/{idPup}/{type}','Pups\PupsController@delete');


/** Proveedores */
Route::get('providers/getAll','ProvidersController@getAll');

/**Clasificacion */
Route::post('classification/save', 'ClassificationController@save');
Route::get('classification/getAll','ClassificationController@getAll');
Route::get('classification/getAllClass/{type}','ClassificationController@getAllClass');
Route::get('classification/getById/{id}','ClassificationController@getById');
Route::put('classification/{id}','ClassificationController@update');
Route::delete('classification/{id}/{type}','ClassificationController@delete');

/**Sensores */
Route::post('sensors/save', 'SensorsController@save');
Route::get('sensors/getAll','SensorsController@getAll');
Route::get('sensors/getById/{id}','SensorsController@getById');
Route::put('sensors/{id}','SensorsController@update');
Route::delete('sensors/{id}/{type}','SensorsController@delete');

/**Cuarentena */
Route::post('quarantine/save', 'QuarantineController@save');
Route::get('quarantine/getAll','QuarantineController@getAll');
Route::get('quarantine/getAllTreatments','QuarantineController@getAllTreatments');
Route::get('quarantine/getAllDiets','QuarantineController@getAllDiets');
Route::get('quarantine/getAllCorrals','QuarantineController@getAllCorrals');
Route::get('quarantine/getById/{id}','QuarantineController@getById');
Route::put('quarantine/{id}','QuarantineController@update');
Route::delete('quarantine/{id}/{type}','QuarantineController@delete');
