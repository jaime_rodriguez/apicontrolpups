-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: app2
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classification`
--

DROP TABLE IF EXISTS `classification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classification`
--

LOCK TABLES `classification` WRITE;
/*!40000 ALTER TABLE `classification` DISABLE KEYS */;
INSERT INTO `classification` VALUES (1,'Sin clasificar',1),(2,'Grasa Tipo 1',1),(3,'Grasa Tipo 2',1);
/*!40000 ALTER TABLE `classification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classpups`
--

DROP TABLE IF EXISTS `classpups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classpups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpup` int(11) DEFAULT NULL,
  `weight` float(10,2) DEFAULT NULL,
  `muscle` int(11) DEFAULT NULL,
  `marbling` int(11) DEFAULT NULL,
  `idclass` int(11) DEFAULT NULL,
  `status` tinyint(11) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `classpups_FK` (`idpup`),
  KEY `classpups_FK_1` (`idclass`),
  CONSTRAINT `classpups_FK` FOREIGN KEY (`idpup`) REFERENCES `pups` (`id`),
  CONSTRAINT `classpups_FK_1` FOREIGN KEY (`idclass`) REFERENCES `classification` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classpups`
--

LOCK TABLES `classpups` WRITE;
/*!40000 ALTER TABLE `classpups` DISABLE KEYS */;
INSERT INTO `classpups` VALUES (1,1,1.00,1,1,3,1),(2,8,15.00,6,1,3,1);
/*!40000 ALTER TABLE `classpups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corral`
--

DROP TABLE IF EXISTS `corral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corral`
--

LOCK TABLES `corral` WRITE;
/*!40000 ALTER TABLE `corral` DISABLE KEYS */;
INSERT INTO `corral` VALUES (1,'CORRAL 1',1),(2,'CORRAL 2',1),(3,'CORRAL 3',1);
/*!40000 ALTER TABLE `corral` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diet`
--

DROP TABLE IF EXISTS `diet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diet`
--

LOCK TABLES `diet` WRITE;
/*!40000 ALTER TABLE `diet` DISABLE KEYS */;
INSERT INTO `diet` VALUES (1,'DIETA 1',1),(2,'DIETA 2',1),(3,'DIETA 3',1);
/*!40000 ALTER TABLE `diet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider`
--

DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider`
--

LOCK TABLES `provider` WRITE;
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
INSERT INTO `provider` VALUES (1,'GM GROUP',1),(2,'Fapsa y Asociados, S.A. de C.V',1);
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pups`
--

DROP TABLE IF EXISTS `pups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `cost` float(10,2) DEFAULT NULL,
  `weight` float(10,2) DEFAULT NULL,
  `supplier` int(100) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `supplier` (`supplier`),
  CONSTRAINT `pups_ibfk_1` FOREIGN KEY (`supplier`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pups`
--

LOCK TABLES `pups` WRITE;
/*!40000 ALTER TABLE `pups` DISABLE KEYS */;
INSERT INTO `pups` VALUES (1,'CRIA 1','prueba 1',1.00,1.00,1,1),(2,'CRIA 2',NULL,NULL,NULL,1,0),(3,'CRIA 3',NULL,NULL,NULL,1,1),(4,'CRIA 4','1',1.00,1.00,1,1),(5,'CRIA 5','12',12.00,12.00,1,1),(6,'CRIA 6','1',1.00,1.00,1,1),(7,'CRIA 7','dasdasd',321321.00,13213.00,1,1),(8,'CRIA 8','FADAS',3213.00,312321.00,2,1);
/*!40000 ALTER TABLE `pups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quarantine`
--

DROP TABLE IF EXISTS `quarantine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quarantine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpup` int(11) DEFAULT NULL,
  `iddiet` int(11) DEFAULT NULL,
  `idtreatment` int(11) DEFAULT NULL,
  `idcorral` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `idpup` (`idpup`),
  KEY `iddiet` (`iddiet`),
  KEY `idcorral` (`idcorral`),
  KEY `idtreatment` (`idtreatment`),
  CONSTRAINT `quarantine_ibfk_1` FOREIGN KEY (`idpup`) REFERENCES `pups` (`id`),
  CONSTRAINT `quarantine_ibfk_2` FOREIGN KEY (`iddiet`) REFERENCES `diet` (`id`),
  CONSTRAINT `quarantine_ibfk_4` FOREIGN KEY (`idcorral`) REFERENCES `corral` (`id`),
  CONSTRAINT `quarantine_ibfk_5` FOREIGN KEY (`idtreatment`) REFERENCES `treatment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quarantine`
--

LOCK TABLES `quarantine` WRITE;
/*!40000 ALTER TABLE `quarantine` DISABLE KEYS */;
INSERT INTO `quarantine` VALUES (1,8,3,2,3,1);
/*!40000 ALTER TABLE `quarantine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `estatus` int(11) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensors`
--

DROP TABLE IF EXISTS `sensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpup` int(11) DEFAULT NULL,
  `heart_rate` int(11) DEFAULT NULL,
  `blood_pressure` int(11) DEFAULT NULL,
  `b_frequency` int(11) DEFAULT NULL,
  `temperature` decimal(10,2) DEFAULT NULL,
  `date_reg` date DEFAULT current_timestamp(),
  `status` smallint(6) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `idpup` (`idpup`),
  CONSTRAINT `sensors_ibfk_1` FOREIGN KEY (`idpup`) REFERENCES `pups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensors`
--

LOCK TABLES `sensors` WRITE;
/*!40000 ALTER TABLE `sensors` DISABLE KEYS */;
INSERT INTO `sensors` VALUES (1,1,75,9,16,38.00,'2022-07-27',0),(2,1,75,9,17,38.00,'2022-07-27',1),(3,8,60,20,25,45.00,'2022-07-27',1),(4,8,65,9,16,39.00,'2022-07-27',1);
/*!40000 ALTER TABLE `sensors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `treatment`
--

DROP TABLE IF EXISTS `treatment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `treatment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `treatment`
--

LOCK TABLES `treatment` WRITE;
/*!40000 ALTER TABLE `treatment` DISABLE KEYS */;
INSERT INTO `treatment` VALUES (1,'TRATAMIENTO 1',1),(2,'TRATAMIENTO 2',1),(3,'TRATAMIENTO 3',1);
/*!40000 ALTER TABLE `treatment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Marianna Barton','angie.bruen@example.net',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','BczYJ6G3hh','2022-07-25 10:31:35','2022-07-25 10:31:35'),(2,'Mireille Raynor','nakia57@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','LP6wo0xTE6','2022-07-25 10:31:35','2022-07-25 10:31:35'),(3,'Deshawn Barton','ckuvalis@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','6jvOxY1rn1','2022-07-25 10:31:35','2022-07-25 10:31:35'),(4,'Dagmar Cummerata','cary.barton@example.net',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','cr3r77Z7yH','2022-07-25 10:31:35','2022-07-25 10:31:35'),(5,'Gabrielle Lubowitz','nolan.mavis@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','vNAhQmjXMJ','2022-07-25 10:31:35','2022-07-25 10:31:35'),(6,'Laura Heaney','mckenzie.enola@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','Pxhj5OuJpr','2022-07-25 10:31:35','2022-07-25 10:31:35'),(7,'Sedrick Hamill','annie50@example.net',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','ms1jTx9WSx','2022-07-25 10:31:35','2022-07-25 10:31:35'),(8,'Jared Purdy','jordon91@example.net',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','66SMTcNK5r','2022-07-25 10:31:35','2022-07-25 10:31:35'),(9,'Frank Larson','gorczany.nettie@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','rts8znozoM','2022-07-25 10:31:35','2022-07-25 10:31:35'),(10,'Clemens Hoeger IV','tillman.suzanne@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','C7NnJGm5RT','2022-07-25 10:31:35','2022-07-25 10:31:35'),(11,'Prof. Darien Dietrich','fjones@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','5RnDBHBxfQ','2022-07-25 10:31:35','2022-07-25 10:31:35'),(12,'Garry Emmerich Jr.','nia84@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','21ur92iYcu','2022-07-25 10:31:35','2022-07-25 10:31:35'),(13,'Imogene Torp','tressie.hand@example.org',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','ByfBJ3fevO','2022-07-25 10:31:35','2022-07-25 10:31:35'),(14,'Mr. Sammie Schowalter','cspinka@example.net',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','3AGhbMMuDe','2022-07-25 10:31:35','2022-07-25 10:31:35'),(15,'Prof. Tyrell Hane IV','rkutch@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','sukoEI8aYS','2022-07-25 10:31:35','2022-07-25 10:31:35'),(16,'Jessy Schroeder','kiara95@example.org',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','eATtkpC7mQ','2022-07-25 10:31:35','2022-07-25 10:31:35'),(17,'Percival Witting IV','ebraun@example.org',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','0NGrjCkyko','2022-07-25 10:31:35','2022-07-25 10:31:35'),(18,'Art Kilback','gmorar@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','o7zQKJySmp','2022-07-25 10:31:35','2022-07-25 10:31:35'),(19,'Ike Deckow IV','runolfsson.hugh@example.net',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','6VGfDhCyES','2022-07-25 10:31:35','2022-07-25 10:31:35'),(20,'Tanya Schoen','aberge@example.com',NULL,'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','96S5QEiLpL','2022-07-25 10:31:35','2022-07-25 10:31:35');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'app2'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-28 22:06:03
