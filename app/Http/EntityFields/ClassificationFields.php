<?php

    namespace App\Http\EntityFields;

    use App\Http\EntityFields\Traits\FieldsTrait;

    class ClassificationFields
    {
        use FieldsTrait;

        private $table = "classpups";

        private $fields = [
            "id" => "id",
            "idpup" => "idpup",
            "weight" => "weight",
            "muscle" => "muscle",
            "marbling" => "marbling",
            "idclass" => "idclass",
            "status" => "status"
        ];

        public function __construct()
        {
            $this->createFieldAS();
        }
    }