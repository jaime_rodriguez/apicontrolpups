<?php
    namespace App\Http\EntityFields\Traits;
    use Illuminate\Support\Facades\DB;

    trait FieldsTrait {
        private $fieldsAS = [];

        private function createFieldAS( ){
            foreach ($this->fields as $key => $value) {
                if( $this->table ){
                    $key = "$this->table.$key";
                }
                $this->fieldsAS[$value] = "$key AS $value";
                $this->{$value} = "$key AS $value";
            }
        }

        public function getFieldsValues(){
            $fields = [];
            foreach ($this->fieldsAS as $key => $value) {
                $fields[] = $value;
            }
            return $fields;
        }

        public function getFieldsValuesAdd( $values ){
            $fields = $this->getFieldsValues();
            foreach ($values as $value) {
                $fields[] = $value;
            }
            return $fields;
        }

        public function getValuesAssingned( $values ){
            $fields = [];
            $_fields = $this->fields;
            foreach ($values as $key => $value) {
                foreach ($_fields as $keyField => $valueField) {
                    if( $key == $valueField ){
                        $fields[$keyField] = $value;
                        unset($_fields[$keyField]);
                        break;
                    }
                }
            }
            return $fields;
        }

        public function as($field, $alias) {
            return DB::Raw("$this->table.$field AS $alias");
        }

        public function max( $field ) {
            $alias = $this->fields[$field];
            return DB::Raw("MAX($this->table.$field) AS $alias");
        }

        public function maxAs( $field, $alias ) {
            return DB::Raw("MAX($this->table.$field) AS $alias");
        }

        public function min( $field ) {
            $alias = $this->fields[$field];
            return DB::Raw("MIN($this->table.$field) AS $alias");
        }

        public function minAs( $field, $alias ) {
            return DB::Raw("MIN($this->table.$field) AS $alias");
        }

        public function sum( $field ) {
            $alias = $this->fields[$field];
            return DB::Raw("SUM($this->table.$field) AS $alias");
        }

        public function sumAs( $field, $alias ) {
            return DB::Raw("SUM($this->table.$field) AS $alias");
        }

        public function count( $field ) {
            $alias = $this->fields[$field];
            return DB::Raw("COUNT($this->table.$field) AS $alias");
        }

        public function countAs( $field, $alias ) {
            return DB::Raw("COUNT($this->table.$field) AS $alias");
        }
    }
