<?php

    namespace App\Http\EntityFields;

    use App\Http\EntityFields\Traits\FieldsTrait;

    class SensorsFields
    {
        use FieldsTrait;

        private $table = "sensors";

        private $fields = [
            "id" => "id",
            "idpup" => "idpup",
            "heart_rate" => "heart_rate",
            "blood_pressure" => "blood_pressure",
            "b_frequency" => "b_frequency",
            "temperature" => "temperature",
            "date_reg" => "date_reg",
            "status" => "status"
        ];

        public function __construct()
        {
            $this->createFieldAS();
        }
    }