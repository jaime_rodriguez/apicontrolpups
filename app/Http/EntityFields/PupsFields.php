<?php

    namespace App\Http\EntityFields;

    use App\Http\EntityFields\Traits\FieldsTrait;

    class PupsFields
    {
        use FieldsTrait;

        private $table = "pups";

        private $fields = [
            "id" => "id",
            "name" => "name",
            "description" => "description",
            "cost" => "cost",
            "weight" => "weight",
            "supplier" => "supplier",
            "status" => "status"
        ];

        public function __construct()
        {
            $this->createFieldAS();
        }
    }