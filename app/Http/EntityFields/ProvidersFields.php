<?php

    namespace App\Http\EntityFields;

    use App\Http\EntityFields\Traits\FieldsTrait;

    class ProvidersFields
    {
        use FieldsTrait;

        private $table = "provider";

        private $fields = [
            "id" => "id",
            "name" => "name",
            "status" => "status"
        ];

        public function __construct()
        {
            $this->createFieldAS();
        }
    }