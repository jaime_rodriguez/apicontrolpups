<?php

    namespace App\Http\EntityFields;

    use App\Http\EntityFields\Traits\FieldsTrait;

    class QuarantineFields
    {
        use FieldsTrait;

        private $table = "quarantine";

        private $fields = [
            "id" => "id",
            "idpup" => "idpup",
            "iddiet" => "iddiet",
            "idtreatment" => "idtreatment",
            "idcorral" => "idcorral",
            "status" => "status"
        ];

        public function __construct()
        {
            $this->createFieldAS();
        }
    }