<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Classification extends Model
{
    protected $table = "classpups";

    protected $fillable = [
        "id",
        "idpup",
        "weight",
        "muscle",
        "marbling",
        "idclass",
        "status"
    ];
    public $timestamps = false;
}