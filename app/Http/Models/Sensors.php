<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Sensors extends Model
{
    protected $table = "sensors";

    protected $fillable = [
        "id",
        "idpup",
        "heart_rate",
        "blood_pressure",
        "b_frequency",
        "temperature",
        "date_reg",
        "status"
    ];
    public $timestamps = false;
}