<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Pups extends Model
{
    protected $table = "pups";

    protected $fillable = [
        "id",
        "name",
        "description",
        "cost",
        "weight",
        "supplier",
        "status"
    ];
    public $timestamps = false;
}