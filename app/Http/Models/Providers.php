<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
    protected $table = "provider";

    protected $fillable = [
        "id",
        "name",
        "status"
    ];
    public $timestamps = false;
}