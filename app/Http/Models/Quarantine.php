<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Quarantine extends Model
{
    protected $table = "quarantine";

    protected $fillable = [
        "id",
        "idpup",
        "iddiet",
        "idtreatment",
        "idcorral",
        "status"
    ];
    public $timestamps = false;
}