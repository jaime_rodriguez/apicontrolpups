<?php 
    namespace App\Http\Services;
    use Illuminate\Support\Facades\DB;

    use App\Http\EntityFields\SensorsFields;
    use App\Http\Models\Sensors;

    class SensorsService{
        private $sensorsModel;
        private $sensorsFields;

        public function __construct()   
        {
            $this->sensorsModel = new Sensors;
            $this->sensorsFields = new SensorsFields;
        }

        public function save($request)
        {
            $dataValues = $this->sensorsFields->getValuesAssingned($request->all());
            return $this->sensorsModel->create($dataValues);
        }

        public function getAll(){
            return $this->sensorsModel->select($this->sensorsFields->getFieldsValuesAdd(
                ["pups.name", "classpups.idclass","classification.name as className"]
            ))
            ->join("pups","pups.id","=","sensors.idpup")
            ->join("classpups","classpups.idpup","=","sensors.idpup")
            ->join("classification","classification.id","=","classpups.idclass")
            ->where('pups.status',1)
            ->where('classpups.idclass',3)
            ->orderBy('sensors.date_reg','desc')
            ->orderBy('sensors.id','desc')
            ->get();
        }

        public function getById($id){
            return $this->sensorsModel->select($this->sensorsFields->getFieldsValuesAdd(
                ["pups.name", "classpups.idclass","classification.name as className"]
            ))
            ->join("pups","pups.id","=","sensors.idpup")
            ->join("classpups","classpups.idpup","=","sensors.idpup")
            ->join("classification","classification.id","=","classpups.idclass")
            ->where('sensors.id',$id)
            ->get();
        }

        public function update($id, $request)
        {
            $dataValues = $this->sensorsFields->getValuesAssingned($request->all());
            // print_r($request->all());
            // print_r($dataValues);
            $finder = $this->sensorsModel->findOrFail($id);
            return $finder->update($dataValues);
        }

        public function delete($id,$type)
        {
            $dataValues = ['status' => $type];
            $finder = $this->sensorsModel->findOrFail($id);
            return $finder->update($dataValues);
        }
    }