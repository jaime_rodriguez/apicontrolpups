<?php 
    namespace App\Http\Services;
    use Illuminate\Support\Facades\DB;

    use App\Http\EntityFields\PupsFields;
    use App\Http\Models\Pups;


    class PupsService{

        private $pupsModel;
        private $pupsFields;

        public function __construct()   
        {
            $this->pupsModel = new Pups;
            $this->pupsFields = new PupsFields;
        }

        public function save($request)
        {
            $dataValues = $this->pupsFields->getValuesAssingned($request->all());
            return $this->pupsModel->create($dataValues);
        }

        public function getAll(){
            return $this->pupsModel->select($this->pupsFields->getFieldsValuesAdd(
                ["provider.name as nameProvider"]
            ))
            ->join("provider","provider.id","=","pups.supplier")
            ->orderBy('id','desc')
            ->get();
        }

        public function getAllNoClass(){
            return $this->pupsModel->select($this->pupsFields->getFieldsValues())
            ->whereRaw("id not in (select idpup from classpups where status=1 )")
            ->orderBy('id','desc')
            ->get();
        }

        public function getAllSick(){
            \DB::statement("SET SQL_MODE=''");
            $sql="select 
                    p.id as idpup,p.name, s.heart_rate ,s.blood_pressure ,s.b_frequency ,s.temperature 
                from pups p 
                join sensors s on s.idpup = p.id and s.id =( select MAX(id) from sensors s2 where s2.idpup = s.idpup and s2.status =1 )
                where p.status = 1 and 
                (s.temperature > 39.5 or s.heart_rate < 70 or s.heart_rate > 80 or s.b_frequency < 15 or s.b_frequency > 20 or s.blood_pressure > 10)";
            $results = DB::select(DB::raw($sql));

            return $results;
        }

        public function getById($id){
            return $this->pupsModel->select($this->pupsFields->getFieldsValues())
            ->where('id',$id)
            ->get();
        }

        public function update($id, $request)
        {
            $dataValues = $this->pupsFields->getValuesAssingned($request->all());
            // print_r($request->all());
            // print_r($dataValues);
            $finder = $this->pupsModel->findOrFail($id);
            return $finder->update($dataValues);
        }

        public function delete($id,$type)
        {
            $dataValues = ['status' => $type];
            $finder = $this->pupsModel->findOrFail($id);
            return $finder->update($dataValues);
        }

    }