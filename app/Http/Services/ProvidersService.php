<?php 
    namespace App\Http\Services;
    use Illuminate\Support\Facades\DB;

    use App\Http\EntityFields\ProvidersFields;
    use App\Http\Models\Providers;


    class ProvidersService{

        private $providersModel;
        private $providersFields;

        public function __construct()   
        {
            $this->providersModel = new Providers;
            $this->providersFields = new ProvidersFields;
        }

        public function getAll(){
            return $this->providersModel->select($this->providersFields->getFieldsValues())
            ->get();
        }
    }