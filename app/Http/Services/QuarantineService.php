<?php 
    namespace App\Http\Services;
    use Illuminate\Support\Facades\DB;

    use App\Http\EntityFields\QuarantineFields;
    use App\Http\Models\Quarantine;

    class QuarantineService{
        private $quarantineModel;
        private $quarantineFields;

        public function __construct()   
        {
            $this->quarantineModel = new Quarantine;
            $this->quarantineFields = new QuarantineFields;
        }

        public function save($request)
        {
            $dataValues = $this->quarantineFields->getValuesAssingned($request->all());
            return $this->quarantineModel->create($dataValues);
        }

        public function getAll(){
            \DB::statement("SET SQL_MODE=''");
            $sql="select 
                    q.*,
                    p.name as pupName,
                    d.description as dietName,
                    t.name as treatmentName,
                    c.description as corralName
                from quarantine q 
                join pups p on q.idpup = p.id
                join diet d on q.iddiet = d.id 
                join treatment t on q.idtreatment = t.id 
                join corral c on q.idcorral = c.id";
            $results = DB::select(DB::raw($sql));

            return $results;
        }

        public function getAllTreatments(){
            \DB::statement("SET SQL_MODE=''");
            $sql="select * from treatment where status = 1";
            $results = DB::select(DB::raw($sql));

            return $results;
        }

        public function getAllDiets(){
            \DB::statement("SET SQL_MODE=''");
            $sql="select * from diet where status = 1";
            $results = DB::select(DB::raw($sql));

            return $results;
        }

        public function getAllCorrals(){
            \DB::statement("SET SQL_MODE=''");
            $sql="select * from corral where status = 1";
            $results = DB::select(DB::raw($sql));

            return $results;
        }

        public function getById($id){
            \DB::statement("SET SQL_MODE=''");
            $sql="select 
                    q.*,
                    p.name,
                    d.description as dietName,
                    t.name as treatmentName,
                    c.description as corralName
                from quarantine q 
                join pups p on q.idpup = p.id
                join diet d on q.iddiet = d.id 
                join treatment t on q.idtreatment = t.id 
                join corral c on q.idcorral = c.id
                where q.id=$id";
            $results = DB::select(DB::raw($sql));

            return $results;;
        }

        public function update($id, $request)
        {
            $dataValues = $this->quarantineFields->getValuesAssingned($request->all());
            // print_r($request->all());
            // print_r($dataValues);
            $finder = $this->quarantineModel->findOrFail($id);
            return $finder->update($dataValues);
        }

        public function delete($id,$type)
        {
            $dataValues = ['status' => $type];
            $finder = $this->quarantineModel->findOrFail($id);
            return $finder->update($dataValues);
        }
    }