<?php 
    namespace App\Http\Services;
    use Illuminate\Support\Facades\DB;

    use App\Http\EntityFields\ClassificationFields;
    use App\Http\Models\Classification;

    class ClassificationService{
        private $classificationModel;
        private $classificationFields;

        public function __construct()   
        {
            $this->classificationModel = new Classification;
            $this->classificationFields = new ClassificationFields;
        }

        public function save($request)
        {
            $dataValues = $this->classificationFields->getValuesAssingned($request->all());
            return $this->classificationModel->create($dataValues);
        }

        public function getAll(){
            \DB::statement("SET SQL_MODE=''");
            $sql="select 
                    p.name,cl.name as class,c.* 
                from classpups c
                join pups p on c.idpup = p.id
                join classification cl on c.idclass = cl.id ";
            $results = DB::select(DB::raw($sql));

            return $results;
        }

        public function getAllClass($type){
            return $this->classificationModel->select($this->classificationFields->getFieldsValuesAdd(
                ["pups.name"]
            ))
            ->join("pups","pups.id","=","classpups.idpup")
            ->where('classpups.idclass',$type)
            // ->toSql();
            ->get();
        }

        public function getById($id){
            return $this->classificationModel->select($this->classificationFields->getFieldsValuesAdd(
                ["classification.name as className","pups.name"]
            ))
            ->join("classification","classification.id","=","classpups.idclass")
            ->join("pups","pups.id","=","classpups.idpup")
            ->where('classpups.id',$id)
            ->get();
        }

        public function update($id, $request)
        {
            $dataValues = $this->classificationFields->getValuesAssingned($request->all());
            // print_r($request->all());
            // print_r($dataValues);
            $finder = $this->classificationModel->findOrFail($id);
            return $finder->update($dataValues);
        }

        public function delete($id,$type)
        {
            $dataValues = ['status' => $type];
            $finder = $this->classificationModel->findOrFail($id);
            return $finder->update($dataValues);
        }
    }