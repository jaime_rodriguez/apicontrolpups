<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\SensorsService;

class SensorsController extends Controller
{
    private $sensorsService;

    public function __construct()
    {
        $this->sensorsService = new SensorsService;
    }

    public function save(Request $request){
        try{
            
            $res = $this->sensorsService->save($request);

            return response()->json([
                'success' => true,
                'data' => [$res],
                'message' => 'Registro Exitoso'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }

    public function getAll(){
        $res = $this->sensorsService->getAll();

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function getById($id){
        $res = $this->sensorsService->getById($id);

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function update($id, Request $request){
        try{
            
            $res = $this->sensorsService->update($id,$request);

            return response()->json([
                'success' => true,
                'data' => [$res],
                "message" =>'Edicion Exitosa'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }

    public function delete($id,$type){
        try{
            $res = $this->sensorsService->delete($id,$type);
            $mss = ($type == 1)?'Activado exitosamente': 'Inactivado exitosamente';
            return response()->json([
                'success' => true,
                'data' => [$res],
                "message" =>$mss
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }
}
