<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\QuarantineService;

class QuarantineController extends Controller
{
    private $quarantineService;

    public function __construct()
    {
        $this->quarantineService = new QuarantineService;
    }

    public function save(Request $request){
        try{
            
            $res = $this->quarantineService->save($request);

            return response()->json([
                'success' => true,
                'data' => [$res],
                'message' => 'Registro Exitoso'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }

    public function getAll(){
        $res = $this->quarantineService->getAll();

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function getAllTreatments(){
        $res = $this->quarantineService->getAllTreatments();

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function getAllDiets(){
        $res = $this->quarantineService->getAllDiets();

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function getAllCorrals(){
        $res = $this->quarantineService->getAllCorrals();

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function getById($id){
        $res = $this->quarantineService->getById($id);

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function update($id, Request $request){
        try{
            
            $res = $this->quarantineService->update($id,$request);

            return response()->json([
                'success' => true,
                'data' => [$res],
                "message" =>'Edicion Exitosa'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }

    public function delete($id,$type){
        try{
            $res = $this->quarantineService->delete($id,$type);
            $mss = ($type == 1)?'Activado exitosamente': 'Inactivado exitosamente';
            return response()->json([
                'success' => true,
                'data' => [$res],
                "message" =>$mss
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }
}
