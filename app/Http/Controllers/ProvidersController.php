<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\ProvidersService;

class ProvidersController extends Controller
{
    private $providersService;

    public function __construct()
    {
        $this->providersService = new ProvidersService;
    }

    public function getAll(){
        $res = $this->providersService->getAll();

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }
}
