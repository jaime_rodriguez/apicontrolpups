<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\ClassificationService;

class ClassificationController extends Controller
{
    private $classificationService;

    public function __construct()
    {
        $this->classificationService = new ClassificationService;
    }

    public function save(Request $request){
        try{
            
            $res = $this->classificationService->save($request);

            return response()->json([
                'success' => true,
                'data' => [$res],
                'message' => 'Registro Exitoso'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }

    public function getAll(){
        $res = $this->classificationService->getAll();

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    
    public function getAllClass($type){
        $res = $this->classificationService->getAllClass($type);

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function getById($id){
        $res = $this->classificationService->getById($id);

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function update($id, Request $request){
        try{
            
            $res = $this->classificationService->update($id,$request);

            return response()->json([
                'success' => true,
                'data' => [$res],
                "message" =>'Edicion Exitosa'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }

    public function delete($id,$type){
        try{
            $res = $this->classificationService->delete($id,$type);
            $mss = ($type == 1)?'Activado exitosamente': 'Inactivado exitosamente';
            return response()->json([
                'success' => true,
                'data' => [$res],
                "message" =>$mss
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }
}
