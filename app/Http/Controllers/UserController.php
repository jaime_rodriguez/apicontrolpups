<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    //
    public function index(){
        return User::all();
    }

    public function user(Request $request){
        return $request->user();
    }
}
