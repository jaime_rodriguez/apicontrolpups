<?php

namespace App\Http\Controllers\Pups;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Services\PupsService;

class PupsController extends Controller
{   
    private $pupsService;

    public function __construct()
    {
        $this->pupsService = new PupsService;
    }



    public function save(Request $request){
        try{
            
            $res = $this->pupsService->save($request);

            return response()->json([
                'success' => true,
                'data' => [$res],
                'message' => 'Registro Exitoso'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }

    public function getAll(){
        $res = $this->pupsService->getAll();

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function getAllNoClass(){
        $res = $this->pupsService->getAllNoClass();

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function getAllSick(){
        $res = $this->pupsService->getAllSick();

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function getById($id){
        $res = $this->pupsService->getById($id);

        return response()->json([
            "success" => true,
            "data" => $res
        ]);
    }

    public function update($id, Request $request){
        try{
            
            $res = $this->pupsService->update($id,$request);

            return response()->json([
                'success' => true,
                'data' => [$res],
                "message" =>'Edicion Exitosa'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }

    public function delete($id,$type){
        try{
            $res = $this->pupsService->delete($id,$type);
            $mss = ($type == 1)?'Activado exitosamente': 'Inactivado exitosamente';
            return response()->json([
                'success' => true,
                'data' => [$res],
                "message" =>$mss
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "success" => false,
                "data" =>  [],
                "message" => $th->getMessage()
            ]);
        }
    }
    
}
